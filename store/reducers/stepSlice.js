import { createSlice } from '@reduxjs/toolkit';

const stepSlice = createSlice({
    name: 'step',
    initialState: {
        step: {
            one: {
                isOkay: true,
                name: "",
                email: "",
                phoneNumber: ""
            },
            two: {
                isOkay: false
            },
            three: {
                isOkay: false
            },
            four: {
                isOkay: false
            }
        },
        currentStep: 1,
        isMonthly: true,
        plan: null,
        pickAddOns: []
    },
    reducers: {
        addStep: (state, action) => {
            const { payload } = action;
            state.step[payload.index][payload.key] = payload.data;
        },
        nextStep: (state, action) => {
            state.currentStep = action.payload
        },
        monthlyUpdate: (state, action) => {
            state.isMonthly = action.payload
        },
        selectPlan: (state, action) => {
            state.plan = action.payload
        },
        selectsAddOns: (state, action) => {
            const { type, data } = action.payload
            if (type == "add") {
                if (!state.pickAddOns) {
                    state.pickAddOns = [data]
                } else {
                    let newPickAddOns = [...state.pickAddOns];
                    newPickAddOns.push(data)
                    state.pickAddOns = newPickAddOns
                }
            }
            if (type === "remove") {
                const newState = state.pickAddOns.filter((item) => item.id !== data.id)
                state.pickAddOns = newState
            }
        }
    },
});

export const { addStep, nextStep, monthlyUpdate, selectPlan, selectsAddOns } = stepSlice.actions;
export default stepSlice.reducer;