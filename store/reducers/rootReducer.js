import { combineReducers } from '@reduxjs/toolkit';
import stepSlice from './stepSlice'; // Örnek olarak bir counterSlice varsayalım

const rootReducer = combineReducers({
    step: stepSlice,

});

export default rootReducer;