import React, { useState } from "react";
import MobileBg from "@/app/assets/images/bg-sidebar-mobile.svg";
import ThankYou from "@/app/assets/images/icon-thank-you.svg";
import { useDispatch, useSelector } from "react-redux";
import { addStep, nextStep } from "@/store/reducers/stepSlice";
import StepOne from "./StepOne";
import StepTwo from "./StepTwo";
import StepThree from "./StepThree";
import StepFour from "./StepFour";
import Step from "../Step";
import toast from "react-hot-toast";
export default function Mobile() {
  const { step } = useSelector((state) => state.step);
  const { plan } = useSelector((state) => state.step);
  const [field, setField] = useState("");
  const { currentStep } = useSelector((state) => state.step);
  const dispatch = useDispatch();
  const [finish, setFinish] = useState(false);

  const nextSteps = () => {
    if (currentStep == 1) {
      if (!step.one.name || !step.one.email || !step.one.phoneNumber) {
        setField("This field is required");
      } else {
        dispatch(nextStep(2));
        dispatch(addStep({ index: "one", key: "isOkay", data: false }));
        dispatch(addStep({ index: "two", key: "isOkay", data: true }));
      }
    }
    if (currentStep == 2) {
      if (plan) {
        dispatch(nextStep(3));
        dispatch(addStep({ index: "two", key: "isOkay", data: false }));
        dispatch(addStep({ index: "three", key: "isOkay", data: true }));
      } else {
        toast.error("Please select a package!");
      }
    }
    if (currentStep == 3) {
      dispatch(nextStep(4));
      dispatch(addStep({ index: "three", key: "isOkay", data: false }));
      dispatch(addStep({ index: "four", key: "isOkay", data: true }));
    }
  };
  const backSteps = () => {
    if (currentStep == 2) {
      dispatch(nextStep(1));
      dispatch(addStep({ index: "two", key: "isOkay", data: false }));
      dispatch(addStep({ index: "one", key: "isOkay", data: true }));
    }
    if (currentStep == 3) {
      dispatch(nextStep(2));
      dispatch(addStep({ index: "three", key: "isOkay", data: false }));
      dispatch(addStep({ index: "two", key: "isOkay", data: true }));
    }
    if (currentStep == 4) {
      dispatch(nextStep(3));
      dispatch(addStep({ index: "four", key: "isOkay", data: false }));
      dispatch(addStep({ index: "three", key: "isOkay", data: true }));
    }
  };

  return (
    <div className='w-full flex flex-col justify-between items-center h-screen relative'>
      <img src={MobileBg.src} className='w-full' alt='' />
      <div className='absolute top-10 '>
        <div className='flex justify-center '>
          <Step
            step={1}
            device='mobile'
            name='YOUR INFO'
            isOkay={step.one.isOkay}
          />
          <Step
            step={2}
            device='mobile'
            name='SELECT PLAN'
            isOkay={step.two.isOkay}
          />
          <Step
            step={3}
            device='mobile'
            name='ADD-ONS'
            isOkay={step.three.isOkay}
          />
          <Step
            step={4}
            device='mobile'
            name='SUMMARY'
            isOkay={step.four.isOkay}
          />
        </div>

        {!finish ? (
          <div>
            {step.one.isOkay && <StepOne field={field} />}
            {step.two.isOkay && <StepTwo />}
            {step.three.isOkay && <StepThree />}
            {step.four.isOkay && <StepFour />}
          </div>
        ) : (
          <div className='mx-6 bg-white px-6 py-20 rounded-lg flex flex-col justify-center items-center'>
            <img src={ThankYou.src} alt='' />
            <h3 className='mt-4 text-2xl font-bold'>Thank you!</h3>
            <p className='text-center mt-4 text-sm text-cool-gray'>
              Thanks for comfirming your subscription! We hope you have fun
              using our platform. If you ever need support, please feel free to
              email us at suuport@loremgaming.com.
            </p>
          </div>
        )}
      </div>

      {!finish && (
        <div className='w-full flex items-center justify-between px-5 h-[72px] bg-white'>
          {currentStep > 1 ? (
            <button
              className='hover:text-marine py-2 px-4 rounded-md text-cool-gray font-[600] text-[14px]'
              onClick={backSteps}
            >
              Go Back
            </button>
          ) : (
            <div></div>
          )}
          {!step.four.isOkay ? (
            <button
              className='bg-marine py-2 px-4 rounded-md text-white text-[14px]'
              onClick={nextSteps}
            >
              Next Step
            </button>
          ) : (
            <button
              className='bg-puplis py-2 px-4 rounded-md text-white text-[14px]'
              onClick={() => setFinish(true)}
            >
              Confirm
            </button>
          )}
        </div>
      )}
    </div>
  );
}
