import { addStep, nextStep } from "@/store/reducers/stepSlice";
import React from "react";
import { useDispatch, useSelector } from "react-redux";

export default function StepFour() {
  const data = useSelector((state) => state.step);
  const { isMonthly } = useSelector((state) => state.step);
  const dispatch = useDispatch();
  const backPlan = () => {
    dispatch(nextStep(2));
    dispatch(addStep({ index: "four", key: "isOkay", data: false }));
    dispatch(addStep({ index: "two", key: "isOkay", data: true }));
  };
  const total =
    parseInt(data?.plan?.price) +
    data.pickAddOns.reduce((acc, cur) => acc + parseInt(cur.price), 0);

  return (
    <div className='flex flex-col h-full mt-10 bg-white p-6 mx-6 rounded-lg'>
      <div>
        <h1 className='text-[30px] text-marine font-bold'>Finishing up</h1>
        <p className='text-cool-gray'>
          Double-check everything looks OK before confirming.
        </p>
      </div>
      <div className='mt-10 flex-1'>
        <div className='p-4 bg-magnolia rounded-lg'>
          <div className='flex justify-between items-center pb-5'>
            <div className='flex flex-col'>
              <span className='text-marine font-bold'>
                {data?.plan?.name} ({isMonthly ? "Monthly" : "Yearly"})
              </span>
              <span
                className='text-sm text-cool-gray underline hover:text-puplis cursor-pointer'
                onClick={backPlan}
              >
                Change
              </span>
            </div>
            <p className='text-marine font-bold'>
              ${data?.plan?.price}/{isMonthly ? "mo" : "yr"}
            </p>
          </div>
          <hr />
          <div className='flex flex-col justify-start items-start pt-5 w-full'>
            {data?.pickAddOns?.map((item) => {
              return (
                <div className='flex justify-between w-full mb-2'>
                  <div className='flex flex-col'>
                    <span className='text-sm text-cool-gray'>{item?.name}</span>
                  </div>
                  <p className='text-marine text-sm font-medium'>
                    +${item?.price}/{isMonthly ? "mo" : "yr"}
                  </p>
                </div>
              );
            })}
          </div>
        </div>
        <div className='mt-1 p-4'>
          <div className='flex justify-between w-full mb-2'>
            <div className='flex flex-col'>
              <span className='text-sm text-cool-gray'>
                Total (per {isMonthly ? "month" : "year"})
              </span>
            </div>
            <p className='text-puplis text-lg font-bold'>
              +${total}/{isMonthly ? "mo" : "yr"}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
