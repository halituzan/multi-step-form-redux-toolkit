import React from "react";
import TextInput from "../TextInput";
import { useDispatch, useSelector } from "react-redux";
import { addStep } from "@/store/reducers/stepSlice";

export default function StepOne({ field }) {
  const dispatch = useDispatch();
  const { step } = useSelector((state) => state.step);
  console.log(step);

  const changeHandler = (e) => {
    dispatch(
      addStep({ index: "one", key: e.target.name, data: e.target.value })
    );
  };
  return (
    <div className='flex flex-col h-full mt-10  bg-white justify-center mx-6 p-6 rounded-lg'>
      <div>
        <h1 className='text-[30px] text-marine font-bold'>Personal Info</h1>
        <p className='text-cool-gray flex-1'>
          Please provide your name, email address, add phone number.
        </p>
      </div>
      <div className='mt-10 flex-1'>
        <TextInput
          type='text'
          placeholder='e.g. Stephen King'
          label='Name'
          name='name'
          value={step.one.name}
          field={field}
          changeHandle={changeHandler}
        />
        <TextInput
          mt='mt-6'
          type='text'
          placeholder='e.g. stephenking@lorem.com'
          label='Email Address'
          name='email'
          value={step.one.email}
          field={field}
          changeHandle={changeHandler}
        />
        <TextInput
          mt='mt-6'
          type='text'
          placeholder='e.g. +1 234 567 890'
          label='Phone Number'
          value={step.one.phoneNumber}
          field={field}
          name='phoneNumber'
          changeHandle={changeHandler}
        />
      </div>
    </div>
  );
}
