import React from "react";

export default function TextInput({
  label,
  placeholder,
  type,
  mt = "mt-0",
  name,
  changeHandle,
  field,
  value,
}) {
  return (
    <div className={`flex flex-col ${mt}`}>
      <div className='flex justify-between items-center'>
        <label htmlFor='name' className='text-marine font-[600]'>
          {label}
        </label>
        <span>
          {field && !value ? (
            <span className='text-strawberry font-bold text-xs'> {field} </span>
          ) : (
            ""
          )}
        </span>
      </div>
      <input
        name={name}
        onChange={(e) => changeHandle(e)}
        type={type}
        value={value}
        placeholder={placeholder}
        className={`p-2 rounded-md border hover:border-marine focus:border-marine hover:cursor-pointer ${field && !value ? "border-strawberry" : ""}`}
      />
    </div>
  );
}
