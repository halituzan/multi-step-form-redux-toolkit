import React from "react";
import Arcade from "@/app/assets/images/icon-arcade.svg";
import Advanced from "@/app/assets/images/icon-advanced.svg";
import Pro from "@/app/assets/images/icon-pro.svg";
import { useDispatch, useSelector } from "react-redux";
import { monthlyUpdate, selectPlan } from "@/store/reducers/stepSlice";

export default function StepTwo() {
  const { isMonthly } = useSelector((state) => state.step);
  const { plan } = useSelector((state) => state.step);
  const dispatch = useDispatch();
  const planUpdate = (e) => {
    dispatch(monthlyUpdate(!e.currentTarget.checked));
    dispatch(selectPlan(null));
  };
  const planSelect = (item) => {
    dispatch(selectPlan(item));
  };
  return (
    <div className='flex flex-col h-full mt-10'>
      <div>
        <h1 className='text-[30px] text-marine font-bold'>Select your plan</h1>
        <p className='text-cool-gray'>
          You have the option of monthly or yearly billing.
        </p>
      </div>
      <div className='mt-10 flex-1'>
        <div className='flex w-full gap-1 justify-between'>
          {isMonthly
            ? monthlyPlans.map((item) => {
                return (
                  <div
                    onClick={() => planSelect(item)}
                    key={item.id}
                    className={`rounded-md border cursor-pointer hover:border-marine min-h-[152px] mx-2 flex-1 p-2 flex flex-col justify-between ${
                      plan?.id === item.id
                        ? "border-marine bg-magnolia"
                        : "border-cool-gray bg-transparent"
                    }`}
                  >
                    <img
                      src={item.icon}
                      alt=''
                      className='min-w-[40px] min-h-[40px] w-10 h-10'
                    />
                    <div className='flex flex-col'>
                      <span className='font-bold text-marine'>{item.name}</span>
                      <span className='text-sm text-cool-gray'>
                        ${item.price}/mo
                      </span>
                    </div>
                  </div>
                );
              })
            : yearlyPlans.map((item) => {
                return (
                  <div
                    onClick={() => planSelect(item)}
                    key={item.id}
                    className={`rounded-md border cursor-pointer hover:border-marine  min-h-[152px] mx-2 flex-1 p-2 flex flex-col justify-between ${
                      plan?.id === item.id
                        ? "border-marine bg-magnolia"
                        : "border-cool-gray bg-transparent"
                    }`}
                  >
                    <img
                      src={item.icon}
                      alt=''
                      className='min-w-[40px] min-h-[40px] w-10 h-10'
                    />
                    <div className='flex flex-col'>
                      <span className='font-bold text-marine'>{item.name}</span>
                      <span className='text-sm  text-cool-gray'>
                        ${item.price}/yr
                      </span>
                      <span className='text-sm font-medium text-marine'>
                        {item.subText}
                      </span>
                    </div>
                  </div>
                );
              })}
        </div>
        <div className='w-full p-2 flex justify-center items-center bg-magnolia mt-10 rounded-md'>
          <label className='inline-flex items-center cursor-pointer'>
            <span
              className={`me-3 text-sm font-medium ${
                isMonthly ? "text-marine" : "text-cool-gray"
              }`}
            >
              Monthly
            </span>
            <input
              type='checkbox'
              defaultChecked={!isMonthly}
              value=''
              className='sr-only peer'
              onChange={planUpdate}
            />
            <div className="relative w-8 h-4 bg-marine peer-focus:outline-none rounded-full peer peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-3 after:w-[14px] after:transition-all peer-checked:bg-marine"></div>
            <span
              className={`ms-3 text-sm font-medium ${
                isMonthly ? "text-cool-gray" : "text-marine"
              }`}
            >
              Yearly
            </span>
          </label>
        </div>
      </div>
    </div>
  );
}

const monthlyPlans = [
  {
    id: 1,
    name: "Arcade",
    price: "9",
    icon: Arcade.src,
  },
  {
    id: 2,
    name: "Advanced",
    price: "12",
    icon: Advanced.src,
  },
  {
    id: 3,
    name: "Pro",
    price: "15",
    icon: Pro.src,
  },
];
const yearlyPlans = [
  {
    id: 1,
    name: "Arcade",
    price: "90",
    icon: Arcade.src,
    subText: "2 months free",
  },
  {
    id: 2,
    name: "Advanced",
    price: "120",
    icon: Advanced.src,
    subText: "2 months free",
  },
  {
    id: 3,
    name: "Pro",
    price: "150",
    icon: Pro.src,
    subText: "2 months free",
  },
];
