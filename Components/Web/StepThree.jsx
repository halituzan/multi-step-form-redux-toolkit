import { selectsAddOns } from "@/store/reducers/stepSlice";
import { useDispatch, useSelector } from "react-redux";

export default function StepThree() {
  const { isMonthly } = useSelector((state) => state.step);
  const { pickAddOns } = useSelector((state) => state.step);


  const dispatch = useDispatch();

  const handleChange = (e, item) => {
    if (e.currentTarget.checked) {
      dispatch(selectsAddOns({ type: "add", data: item }));
    } else {
      dispatch(selectsAddOns({ type: "remove", data: item }));
    }
  };
  const handleChangeDiv = (id) => {
    const input = document.getElementById(id + "input");
    input.click();
  };

  return (
    <div className='flex flex-col h-full mt-10'>
      <div>
        <h1 className='text-[30px] text-marine font-bold'>Pick add-ons</h1>
        <p className='text-cool-gray'>
          Add-ons help enhance your gaming experience.
        </p>
      </div>
      <div className='mt-10 flex-1'>
        <div className='flex flex-col'>
          {isMonthly
            ? pickAddOnsMonthly.map((item) => {
              const isPick = pickAddOns.some((i) => i.id === item.id);

              return (
                <div
                  className={`w-full hover:border-marine cursor-pointer flex justify-between items-center mb-5 border  rounded-lg p-4
                    ${isPick
                      ? "border-marine bg-magnolia"
                      : "border-cool-light bg-transparent"
                    }
                    `}
                  onClick={() => handleChangeDiv(item.id)}
                >
                  <input
                    id={item.id + "input"}
                    type='checkbox'
                    value=''
                    defaultChecked={isPick}
                    onChange={(e) => handleChange(e, item)}
                    className='w-4 h-4 text-puplis mr-4 bg-puplis border-puplis rounded  '
                  />

                  <div className='flex-1 flex flex-col'>
                    <span className='text-marine font-bold'>{item.name}</span>
                    <span className='text-sm text-cool-gray'>
                      {item.subText}
                    </span>
                  </div>
                  <div>
                    <p className='text-puplis text-sm font-medium'>
                      +${item.price}/{isMonthly ? "mo" : "yr"}
                    </p>
                  </div>
                </div>
              );
            })
            : pickAddOnsYearly.map((item) => {
              const isPick = pickAddOns.some((i) => i.id === item.id);
              return (
                <div
                  className={`w-full hover:border-marine cursor-pointer flex justify-between items-center mb-5 border  rounded-lg p-4
                  ${isPick
                      ? "border-marine bg-magnolia"
                      : "border-cool-light bg-transparent"
                    }
                  `}
                  onClick={() => handleChangeDiv(item.id)}
                >
                  <div className='mr-4'>
                    <input
                      id={item.id + "input"}
                      onChange={(e) => handleChange(e, item)}
                      type='checkbox'
                      defaultChecked={isPick}
                      value=''
                      className='w-4 h-4 text-puplis bg-puplis border-puplis rounded  '
                    />
                  </div>
                  <div className='flex-1 flex flex-col'>
                    <span className='text-marine font-bold'>{item.name}</span>
                    <span className='text-sm text-cool-gray'>
                      {item.subText}
                    </span>
                  </div>
                  <div>
                    <p className='text-puplis text-sm font-medium'>
                      +${item.price}/{isMonthly ? "mo" : "yr"}
                    </p>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
}

const pickAddOnsMonthly = [
  {
    id: 1,
    name: "Online service",
    subText: "Access to multiplayers games",
    price: "1",
  },
  {
    id: 2,
    name: "Larger storage",
    subText: "Extra 1TB of cloud save",
    price: "2",
  },
  {
    id: 3,
    name: "Customizable profile",
    subText: "Custom theme on your profile",
    price: "2",
  },
];
const pickAddOnsYearly = [
  {
    id: 1,
    name: "Online service",
    subText: "Access to multiplayers games",
    price: "10",
  },
  {
    id: 2,
    name: "Larger storage",
    subText: "Extra 1TB of cloud save",
    price: "20",
  },
  {
    id: 3,
    name: "Customizable profile",
    subText: "Custom theme on your profile",
    price: "20",
  },
];
