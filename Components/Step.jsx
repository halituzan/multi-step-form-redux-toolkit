import React from "react";

export default function Step({ step, name, isOkay, device = "web" }) {
  return (
    <div
      className={`flex justify-start items-center mb-6 ${
        device == "mobile" && "mx-3"
      }`}
    >
      <div
        className={`min-w-[35px] min-h-[35px] border-light border flex justify-center items-center rounded-full ${
          isOkay ? "bg-light" : "bg-transparent text-white"
        }`}
      >
        {step}
      </div>
      {device === "web" && (
        <div className='flex flex-col ml-4 w-32 text-white'>
          <span className='text-[12px] text-light'>STEP {step}</span>
          <span className='text-[13px] font-bold'>{name}</span>
        </div>
      )}
    </div>
  );
}
