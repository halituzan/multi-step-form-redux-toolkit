import React from "react";
import Web from "../Web/Web";
import Mobile from "../Mobile/Mobile";

export default function Main() {
  return (
    <main className='bg-light-gray w-screen min-h-screen flex justify-center items-start md:items-center'>
      <div className='md:flex justify-center items-center hidden w-full '>
        <Web />
      </div>
      <div className='md:hidden flex w-full'>
        <Mobile />
      </div>
    </main>
  );
}
