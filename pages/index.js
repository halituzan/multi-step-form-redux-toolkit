import { Provider } from "react-redux";
import { store } from "../store";
import Main from "@/Components/Main/Main";
import "../app/globals.css"
import toast, { Toaster } from 'react-hot-toast';
function App() {
  return (
    <Provider store={store}>
      <Main />
      <Toaster />
    </Provider>
  );
}

export default App;
